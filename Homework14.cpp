﻿
#include <iostream>
#include <time.h>


int main()
{
	// модуль даты
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	// вводим характеристики двумерного массива
	const int line = 7;
	const int column = 7;
	int array[line][column];
	
	// заполняем массив и выводим в консоль
	for (int i {0}; i <= line-1; i++)
	{
		for (int j {0}; j <= column-1; j++)
		{ 
			array[i][j] = i + j;
			std::cout << "  " << array[i][j] << "  ";
		}
		std::cout << "\n";
	} 
	std::cout << "\n";

	// получаем остаток от деления числа календаря на номер строки и выводим сумму элементов строки остатка
	int reminder = buf.tm_mday % line;
	int summ { 0 };
	for (int j{ 0 }; j <= column - 1; j++)
	{
		summ = summ + array[reminder][j];
	}
	std::cout << " Summ of " << reminder+1 << " line is " << summ << "\n";
}
